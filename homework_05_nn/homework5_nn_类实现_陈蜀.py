import numpy as np
from sklearn import datasets, linear_model
from matplotlib import pyplot as plt
from sklearn.metrics import accuracy_score

# 生成数据
np.random.seed(0)
X, y = datasets.make_moons(200, noise=0.20)
y_true = np.array(y).astype(float)

# 生成输出目标
t = np.zeros((X.shape[0], 2))
t[np.where(y==0), 0] = 1
t[np.where(y==1), 1] = 1

#sigmod函数及其导数
def sigmod(z):
    return 1.0/(1+np.exp(-z))

class NN_model:
    def __init__(self,nodes=None):
        self.alpha = 0.01 #学习速率
        self.n = 1000 #迭代次数 
        if not nodes:
            self.nodes = [2, 6, 2]          # default nodes size (from input -> output)
        else:
            self.nodes = nodes

    #初始化权重
    def init_weight(self):
        W = []
        B = []
        n_layer = len(self.nodes)
        for i in range(n_layer-1):
            w = np.random.randn(self.nodes[i], self.nodes[i+1]) / np.sqrt(self.nodes[i])
            b = np.random.randn(1, self.nodes[i+1])
            W.append(w)
            B.append(b)    
        self.W = W
        self.B = B
    
    #正向计算
    def forward(self, X):
        Z = []
        x0 = X
        for i in range(len(self.nodes)-1):
            z = sigmod(np.dot(x0, self.W[i]) + self.B[i])
            x0 = z
            Z.append(z)
        
        self.Z = Z
        return Z[-1]
    
    #反向误差传播
    def backpropagation(self, X, y, n=None, alpha=None):
        if not n: n = self.n
        if not alpha: alpha = self.alpha
        self.X = X
        self.Y = y
        for i in range(n):
            # 正向计算
            self.forward(X)
            self.evaluate() 
            # 更新权重值
            W = self.W
            B = self.B
            Z = self.Z

            D = []
            d0 = y
            n_layer = len(self.nodes)
            for j in range(n_layer-1, 0, -1):
                jj = j - 1
                z = self.Z[jj]
                if j == n_layer - 1:
                    d = z*(1-z)*(d0 - z)
                else:
                    d = z*(1-z)*np.dot(d0, W[j].T)    
                d0 = d
                D.insert(0, d)
            # 更新权重列表
            for j in range(n_layer-1, 0, -1):
                jj = j - 1
                if jj != 0:
                    W[jj] += alpha * np.dot(Z[jj-1].T, D[jj])
                else:
                    W[jj] += alpha * np.dot(X.T, D[jj])   
                B[jj] += alpha * np.sum(D[jj], axis=0)

    #计算正确率
    def evaluate(self):
        z = self.Z[-1]
        # print loss, accuracy
        L = np.sum((z - self.Y)**2)   
        y_pred = np.argmax(z, axis=1)
        y_true = np.argmax(self.Y, axis=1)
        acc = accuracy_score(y_true, y_pred)*100
        #输出正确率
        print("L=%.4f,正确率 = %.2f" % (L, acc),"%")

nn = NN_model([2, 6, 4, 2])
nn.init_weight()
nn.backpropagation(X, t, 1000)

#预测
y_res  = nn.forward(X)
y_pred = np.argmax(y_res, axis=1)

#可视化
plt.rcParams['font.sans-serif'] = ['SimHei']
plt.rcParams['axes.unicode_minus'] = False

plt.scatter(X[:, 0], X[:, 1], c=y, cmap=plt.cm.Spectral)
plt.title("原始数据")
plt.show()

plt.scatter(X[:, 0], X[:, 1], c=y_pred, cmap=plt.cm.Spectral)
plt.title("预测结果")
plt.show()







