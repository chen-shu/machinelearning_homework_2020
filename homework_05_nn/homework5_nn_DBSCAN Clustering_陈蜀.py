import numpy as np
from sklearn import datasets
from matplotlib import pyplot as plt
from sklearn.cluster import DBSCAN
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import StandardScaler

plt.rcParams['font.sans-serif'] = ['SimHei']
plt.rcParams['axes.unicode_minus'] = False

# 生成数据
np.random.seed(0)
x, y = datasets.make_moons(200, noise=0.20)
y_true = np.array(y).astype(float)

# 原始数据可视化
def origin_show():
    plt.figure()
    plt.scatter(x[:, 0], x[:, 1], c=y, cmap=plt.cm.Spectral)
    plt.title('原始数据')
    plt.show()
    
#origin_show()

#DBSCAN
x = StandardScaler().fit_transform(x)
db = DBSCAN().fit(x)
core_samples_mask = np.zeros_like(db.labels_, dtype=bool)
core_samples_mask[db.core_sample_indices_] = True
labels = db.labels_

#可视化
plt.scatter(x[:, 0], x[:, 1], c=labels, cmap=plt.cm.Spectral)
plt.title("DBSCAN聚类")
plt.show()
