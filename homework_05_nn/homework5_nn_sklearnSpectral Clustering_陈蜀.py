import numpy as np
from sklearn import datasets
from matplotlib import pyplot as plt
from sklearn.cluster import AgglomerativeClustering
from sklearn.metrics import accuracy_score

plt.rcParams['font.sans-serif'] = ['SimHei']
plt.rcParams['axes.unicode_minus'] = False

# 生成数据
np.random.seed(0)
x, y = datasets.make_moons(200, noise=0.20)
#data = 
y_true = np.array(y).astype(float)

# 原始数据可视化
def origin_show():
    plt.figure()
    plt.scatter(x[:, 0], x[:, 1], c=y, cmap=plt.cm.Spectral)
    plt.title('原始数据')
    plt.show()
    
#origin_show()

x.astype('int64')
X = []#空list，用来放置所有点的横坐标
Y = []#空list，用来放置所有点的纵坐标
for p in range(np.shape(x)[0]):
    X.append(x[p][0])
    Y.append(x[p][1])

k=2

sc = AgglomerativeClustering(n_clusters=k, affinity='euclidean',linkage='single')# precomputed,rbf,nearest_neighbors
sc_clusters2 = sc.fit_predict(x)

#可视化
plt.scatter(X,Y, c=sc_clusters2)
plt.title("谱聚类:Spectral Clustering")
plt.show()
