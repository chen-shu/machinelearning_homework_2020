import numpy as np
from sklearn import datasets
from matplotlib import pyplot as plt
from sklearn.metrics import accuracy_score

plt.rcParams['font.sans-serif'] = ['SimHei']
plt.rcParams['axes.unicode_minus'] = False

# 生成数据
np.random.seed(0)
x, y = datasets.make_moons(200, noise=0.20)
y_true = np.array(y).astype(float)

# 生成输出目标
t = np.zeros((x.shape[0], 2))
t[np.where(y==0), 0] = 1
t[np.where(y==1), 1] = 1

# 原始数据可视化
def origin_show():
    plt.figure()
    plt.scatter(x[:, 0], x[:, 1], c=y, cmap=plt.cm.Spectral)
    plt.title('原始数据')
    plt.show()

alpha = 0.01 #学习速率
n = 1000 #迭代次数
n_input_dim = x.shape[1] #输入个数
n_output_dim = 2 #输出个数
n_hide_dim = 4 #隐藏层数

#初始化权重数组
def initial():
    w1 = np.random.randn(n_input_dim, n_hide_dim)/np.sqrt(n_input_dim)
    b1 = np.zeros((1, n_hide_dim))
    w2 = np.random.randn(n_hide_dim, n_output_dim)/np.sqrt(n_hide_dim)
    b2 = np.zeros((1,n_output_dim))
    return w1, b1, w2, b2

#sigmod函数及其导数
def sigmod(z):
    return 1.0/(1+np.exp(-z))

#正向计算
def forward(x, w1, b1, w2, b2):
    z1 = sigmod(x.dot(w1) + b1)
    z2 = sigmod(z1.dot(w2) + b2)
    return z1,z2

def forward_show(x,y_predict):
    plt.figure()
    plt.scatter(x[:, 0], x[:, 1], c=y_predict, cmap=plt.cm.Spectral)
    plt.title('预测结果')
    plt.show()

#错误预测可视化
def error_show(x,y_predict):
    plt.figure()
    plt.title('错误显示')
    error = []
    for i in range(np.shape(x)[0]):
        if y_predict[i] != y[i]:
            error.append(x[i])
    plt.scatter(x[:, 0], x[:, 1], c=y_predict, cmap=plt.cm.Spectral)
    error = np.array(error)
    plt.scatter(error[:,0],error[:,1],c='r')
    #print(error)
    plt.show()

w1, b1, w2, b2 = initial()
z1,z2 = forward(x, w1, b1, w2, b2)
y_predict = np.argmax(z2, axis=1)

#反向误差传播
def backpropagation(x,y):
    accc = []
    w1, b1, w2, b2 = initial()
    z1,z2 = forward(x, w1, b1, w2, b2)
    for i in range(n):
        z1,z2 = forward(x, w1, b1, w2, b2)
        L = np.sum((z2 - y)**2)
        y_predict = np.argmax(z2, axis=1)
        acc = accuracy_score(y_true, y_predict)*100
        accc.append(acc)
        #print("第%4d次迭代, L=%.4f,正确率 = %.2f" % (i+1,L,acc),"%")
        d2 = z2*(1-z2)*(y - z2)
        d1 = z1*(1-z1)*(np.dot(d2, w2.T))
        #更新w1, b1, w2, b2
        w2 += alpha * np.dot(z1.T, d2)
        b2 += alpha * np.sum(d2, axis=0)
        w1 += alpha * np.dot(x.T, d1)
        b1 += alpha * np.sum(d1, axis=0)
    return x, y_predict,accc
   
x, y_predict, accc = backpropagation(x, t)
origin_show() #显示原始数据
forward_show(x,y_predict) #显示分类结果
error_show(x,y_predict) #错误分类可视化