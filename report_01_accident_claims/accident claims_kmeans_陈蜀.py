import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import csv
#sklearn中相关库
from sklearn.cluster import KMeans
from sklearn.metrics import confusion_matrix, accuracy_score

plt.rcParams['font.sans-serif'] = ['SimHei']
plt.rcParams['axes.unicode_minus'] = False

#读取数据
train_dataset = pd.read_csv("E:\\pyhomework\\machinelearning_homework\\report_01_accident_claims\\data\\train.csv") #训练数据集
test_dataset = pd.read_csv("E:\\pyhomework\\machinelearning_homework\\report_01_accident_claims\\data\\test.csv") #测试数据集

#原始数据的处理
train_dataset = train_dataset.drop(['CaseId','Evaluation'],axis=1) #去掉CaseId和Evaluation,得到Qk
test_dataset = test_dataset.drop(['CaseId'],axis=1)
evaluation_true = pd.read_csv("E:\\pyhomework\\machinelearning_homework\\report_01_accident_claims\\data\\train.csv",usecols=['Evaluation']) #读取evaluation,后续可以转换为列表

#K-Means预测
est = KMeans(n_clusters=2, init="k-means++", n_jobs=-1)
est.fit(train_dataset, evaluation_true)

pred_train = est.predict(train_dataset) #训练数据的预测结果
pred_test = est.predict(test_dataset) #测试数据的预测结果

#预测后所得数据的处理
caseid = np.array(range(80000))+200001 #生成CaseId，方便后续和预测结果、结果概率等数据绑定后保存
caseid = pd.DataFrame(caseid,index=None,columns=['CaseId']) #把caseid由array转化为DataFrame
pred_test = pd.DataFrame(pred_test,index=None,columns=['Evaluation']) #把测试数据的预测结果由array转化为DataFrame
submit_data = pd.concat([caseid,pred_test],axis=1) #合并caseid和pred_test

#保存数据
submit_data.to_csv("submit_data_kmeans.csv", index=False)

#预测准确率的计算
acc_train = accuracy_score(evaluation_true,pred_train)*100#准确率
print("训练数据的预测正确率 = %.2f" % acc_train,"%")

#混淆矩阵可视化
def confusion_metrix_show(evaluation_true,pred_train):
    Confusion_matrix=confusion_matrix(evaluation_true,pred_train) #evaluation_true真实值，pred_train预测值
    plt.matshow(Confusion_matrix)
    plt.title("混淆矩阵")
    plt.ylabel("实际类型")
    plt.xlabel("预测类型")
    plt.colorbar()
    plt.show()
confusion_metrix_show(evaluation_true,pred_train)







