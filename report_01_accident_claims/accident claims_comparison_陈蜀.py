import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import csv
#sklearn中相关库
from sklearn.linear_model.logistic import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import confusion_matrix,accuracy_score,roc_curve,auc,precision_recall_curve,average_precision_score

plt.rcParams['font.sans-serif'] = ['SimHei']
plt.rcParams['axes.unicode_minus'] = False

#读取数据
train_dataset = pd.read_csv("E:\\pyhomework\\machinelearning_homework\\report_01_accident_claims\\data\\train.csv") #训练数据集
test_dataset = pd.read_csv("E:\\pyhomework\\machinelearning_homework\\report_01_accident_claims\\data\\test.csv") #测试数据集
#submit_dataset = pd.read_csv("submit_data.csv") #提交的数据

#原始数据的处理
train_dataset = train_dataset.drop(['CaseId','Evaluation'],axis=1) #去掉CaseId和Evaluation,得到Qk
test_dataset = test_dataset.drop(['CaseId'],axis=1)
evaluation_true = pd.read_csv("E:\\pyhomework\\machinelearning_homework\\report_01_accident_claims\\data\\train.csv",usecols=['Evaluation']) #读取evaluation,后续可以转换为列表

#Logistic Regression预测
lr = LogisticRegression()
lr.fit(train_dataset, evaluation_true)

pred_train_l = lr.predict(train_dataset) #训练数据的预测结果
pred_test_l = lr.predict(test_dataset) #测试数据的预测结果
pred_test_probability_l = lr.predict_proba(test_dataset)[:,1] #测试数据的预测结果为1的概率，即获得赔偿的概率。[:,0]是预测结果为0的概率

#随机森林预测
clf = RandomForestClassifier(n_estimators=100, random_state=0)
clf.fit(train_dataset, evaluation_true)

pred_train_r = clf.predict(train_dataset) #训练数据的预测结果
pred_test_r = clf.predict(test_dataset) #测试数据的预测结果
pred_test_probability_r = clf.predict_proba(test_dataset)[:,1] #测试数据的预测结果为1的概率，即获得赔偿的概率。[:,0]是预测结果为0的概率


#绘制PR曲线
y_pro_l = lr.predict_proba(train_dataset)[:,1]
y_pro_r = clf.predict_proba(train_dataset)[:,1]
precision_l, recall_l, thresholds_l = precision_recall_curve(evaluation_true,y_pro_l)
precision_r, recall_r, thresholds_r = precision_recall_curve(evaluation_true,y_pro_r)
plt.figure()
plt.plot(precision_l, recall_l,'m',label='Logistic Regression')
plt.plot(precision_r, recall_r,'c',label='随机森林预测')
plt.legend(loc='lower left')
plt.xlabel('召回率Recall')
plt.ylabel('精度Precision')
plt.grid(True)
plt.xlim([0.18, 1.0])
plt.ylim([0.0, 1.0])
plt.title('PR曲线')
plt.show()

#绘制ROC曲线
false_positive_rate_l, recall_l, thresholds_l = roc_curve(evaluation_true,y_pro_l)
false_positive_rate_r, recall_r, thresholds_r = roc_curve(evaluation_true,y_pro_r)
roc_auc_l = auc(false_positive_rate_l, recall_l)
roc_auc_r = auc(false_positive_rate_r, recall_r)
plt.figure()
plt.title("ROC曲线(Receiver Operating Characteristic)")#verticalalignment='center'→一个规定title格式的参数，个人觉得加上不好看
plt.plot(false_positive_rate_l,recall_l,'m',label='Logistic Regression,AUC=%0.2f'%roc_auc_l)
plt.plot(false_positive_rate_r,recall_r,'c',label='随机森林预测,AUC=%0.2f'%roc_auc_r)
plt.legend(loc='lower right')
plt.plot([0,1],[0,1],'r--')
plt.xlim([0.0,1.0])
plt.ylim([0.0,1.0])
plt.grid(True)
plt.ylabel('召回率Recall(即覆盖程度TPR)')
plt.xlabel('假阳性率false_positive_rate(FPR)')
plt.show()








