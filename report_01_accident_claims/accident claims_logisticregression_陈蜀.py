import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import csv
#sklearn中相关库
from sklearn.linear_model.logistic import LogisticRegression
from sklearn.metrics import confusion_matrix,accuracy_score,roc_curve,auc,precision_recall_curve,average_precision_score

plt.rcParams['font.sans-serif'] = ['SimHei']
plt.rcParams['axes.unicode_minus'] = False

#读取数据
train_dataset = pd.read_csv("E:\\pyhomework\\machinelearning_homework\\report_01_accident_claims\\data\\train.csv") #训练数据集
test_dataset = pd.read_csv("E:\\pyhomework\\machinelearning_homework\\report_01_accident_claims\\data\\test.csv") #测试数据集
#submit_dataset = pd.read_csv("submit_data.csv") #提交的数据

#原始数据的处理
train_dataset = train_dataset.drop(['CaseId','Evaluation'],axis=1) #去掉CaseId和Evaluation,得到Qk
test_dataset = test_dataset.drop(['CaseId'],axis=1)
evaluation_true = pd.read_csv("E:\\pyhomework\\machinelearning_homework\\report_01_accident_claims\\data\\train.csv",usecols=['Evaluation']) #读取evaluation,后续可以转换为列表

#Logistic Regression预测
lr = LogisticRegression()
lr.fit(train_dataset, evaluation_true)

pred_train = lr.predict(train_dataset) #训练数据的预测结果
pred_test = lr.predict(test_dataset) #测试数据的预测结果
pred_test_probability = lr.predict_proba(test_dataset)[:,1] #测试数据的预测结果为1的概率，即获得赔偿的概率。[:,0]是预测结果为0的概率

#预测后所得数据的处理
caseid = np.array(range(80000))+200001 #生成CaseId，方便后续和预测结果、结果概率等数据绑定后保存
caseid = pd.DataFrame(caseid,index=None,columns=['CaseId']) #把caseid由array转化为DataFrame
pred_test = pd.DataFrame(pred_test,index=None,columns=['Evaluation']) #把测试数据的预测结果由array转化为DataFrame
pred_test_probability = pd.DataFrame(pred_test_probability,index=None,columns=['Evaluation']) #把测试数据的预测结果为1的概率由array转化为DataFrame
submit_data = pd.concat([caseid,pred_test],axis=1) #合并caseid和pred_test
probability = pd.concat([caseid,pred_test_probability],axis=1) #合并caseid和pred_test_probability

#保存数据
submit_data.to_csv("submit_data_logisticregression.csv", index=False)
probability.to_csv("my_RF_prediction_logisticregression.csv",index=False)

#预测准确率的计算
acc_train = accuracy_score(evaluation_true,pred_train)*100#准确率
ave_train = average_precision_score(evaluation_true,pred_train)#平均预测分数
print("训练数据的预测正确率 = %.2f" % acc_train,"%")

#混淆矩阵可视化
def confusion_metrix_show(evaluation_true,pred_train):
    Confusion_matrix=confusion_matrix(evaluation_true,pred_train) #evaluation_true真实值，pred_train预测值
    plt.matshow(Confusion_matrix)
    plt.title("混淆矩阵")
    plt.ylabel("实际类型")
    plt.xlabel("预测类型")
    plt.colorbar()
    plt.show()
confusion_metrix_show(evaluation_true,pred_train)

#绘制PR曲线
def pr_show(evaluation_true,train_dataset):
    #roc = roc_auc_score(evaluation_true,pred_train)
    y_pro = lr.predict_proba(train_dataset)[:,1]
    precision, recall, thresholds = precision_recall_curve(evaluation_true,y_pro)
    plt.figure()
    plt.plot(precision, recall,'m')
    plt.xlabel('召回率Recall')
    plt.ylabel('精度Precision')
    plt.grid(True)
    plt.xlim([0.18, 1.0])
    plt.ylim([0.0, 1.0])
    plt.title('PR曲线')
    plt.show()
pr_show(evaluation_true,train_dataset)

#绘制ROC曲线，用于判断模型好坏
def ROC_show(evaluation_true,train_dataset):
    y_pro = lr.predict_proba(train_dataset)[:,1]
    false_positive_rate, recall, thresholds = roc_curve(evaluation_true,y_pro)
    roc_auc = auc(false_positive_rate, recall)
    plt.figure()
    plt.title("ROC曲线(Receiver Operating Characteristic)")#verticalalignment='center'→一个规定title格式的参数，个人觉得加上不好看
    plt.plot(false_positive_rate,recall,'m',label='AUC=%0.2f'%roc_auc)
    plt.legend(loc='lower right')
    plt.plot([0,1],[0,1],'c--')
    plt.xlim([0.0,1.0])
    plt.ylim([0.0,1.0])
    plt.grid(True)
    plt.ylabel('召回率Recall(即覆盖程度TPR)')
    plt.xlabel('假阳性率false_positive_rate(FPR)')
    plt.show()
ROC_show(evaluation_true,train_dataset)







