from sklearn.datasets import load_digits
import numpy as np
import matplotlib.pyplot as plt

#读取数据
digits = load_digits()
'''
digits.data:手写数字特征向量数据集，每一个元素都是一个64维的特征向量
digits.targit:特征向量对应的标记，每个元素都是自然数0-9的数字
digits.images:对应着data中的数据，每一个元素都是8*8的二维数组，其元素代表的是灰度值
'''

#原始数据可视化
fig = plt.figure(figsize=(6, 6))  # figure1的大小为宽、长(单位：inch)
fig.subplots_adjust(left=0, right=1, bottom=0, top=1, hspace=0.05, wspace=0.05)
for i in range(64):
    ax = fig.add_subplot(8, 8, i + 1, xticks=[], yticks=[])#添加子图8*8
    ax.imshow(digits.images[i], cmap=plt.cm.binary)#cmap设置颜色，或cmap=plt.get_cmap('gray_r')
    ax.text(0, 7, str(digits.target[i]), size=20)#标出数字角标

# sigmoid
def sigmoid(z):
    return 1/(1+np.exp(-z))


# 数据训练
def logistic_train(dataset):
    n = np.shape(dataset.data)[1]
    m = 400  #训练个数
    train_data = dataset.data[0:m, :]  #训练样本前200个
    weight = np.ones((10, n))  #回归函数参数10个
    b = np.ones(10)
    times = 1000  #训练1000次
    alpha = 0.01
    for t in range(10):
        label = np.copy(dataset.target[0:m])
        for x in range(m):
            if label[x] == t:  
                label[x] = 1   #1表示“相同”
            else:
                label[x] = 0  #0表示“不同”
        for i in range(times):
            num_index = list(range(m))
            for j in range(m):  #循环，训练
                rand_index = int(np.random.uniform(0, len(num_index)))
                error = label[rand_index] - sigmoid(sum(weight[t] * train_data[rand_index]) + b[t])
                weight[t] += alpha * error * train_data[rand_index]
                b[t] += alpha * error
                del (num_index[rand_index])
    return weight,b


# 数据预测
def logistic_test(dataset):
    weight, b = logistic_train(dataset)
    m = 400
    test_score = 0
    test_len = 64
    test_data = dataset.data[m:m+test_len, :]  #预测数据
    result = np.zeros(test_len)
    for j in range(test_len):
        prob = np.zeros(10)
        for i in range(10):
            prob[i] = sigmoid(sum(weight[i] * test_data[j]) + b[i])
        result[j] = np.argmax(prob)  #确定预测结果
    for index in range(test_len):
        if result[index] == dataset.target[m + index]:
            test_score += 1
    test_score /= test_len
    test_score = test_score*100
    print("正确率: %f" % test_score,"%")
    plot_result(dataset, m, result)

# 结果可视化
def plot_result(dataset, start, result):
    length = np.shape(result)[0]
    fig = plt.figure(figsize=(6, 6))  # figure1的大小为宽、长(单位：inch),和原始数据可视化相同
    fig.subplots_adjust(left=0, right=1, bottom=0, top=1, hspace=0.05, wspace=0.05) #和原始数据可视化相同
    for i in range(length):
        ax = fig.add_subplot(8, 8, i + 1, xticks=[], yticks=[])
        ax.imshow(digits.images[i+start], cmap=plt.cm.binary)
        ax.text(0, 7, str(dataset.target[i+start]), size=20) #原始结果显示成黑色的
        ax.text(6, 7, str(int(result[i])), size=20, color="b") #预测的结果显示成蓝色的
    plt.show()

#运行吧！！！
logistic_test(digits)
plt.show()
