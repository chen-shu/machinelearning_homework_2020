from sklearn.datasets import load_digits
import matplotlib.pyplot as plt
from sklearn.linear_model.logistic import LogisticRegression
from sklearn.metrics import accuracy_score

#读取数据
digits = load_digits()
'''
digits.data:手写数字特征向量数据集，每一个元素都是一个64维的特征向量
digits.targit:特征向量对应的标记，每个元素都是自然数0-9的数字
digits.images:对应着data中的数据，每一个元素都是8*8的二维数组，其元素代表的是灰度值
'''
#数据
m = 400 #训练数量
test = 100 #预测数量
real = digits.target[0:m+test]  #真实数据
x_train = digits.data[:m,:]  #训练数据x     
x_real = real[:m]  #原始值x       
y_train  = digits.data[m:m+test, :]  #预测数据    
y_real  = real[m:m+test]  #原始值y

#数据训练&预测
logisticregression = LogisticRegression()
logisticregression.fit(x_train,x_real)  #训练数据
pre_test = logisticregression.predict(y_train)  #预测数据

#计算正确率
test_score = accuracy_score(y_real, pre_test)*100  #正确率
print("正确率:%f" % test_score,"%")

#可视化
fig = plt.figure(figsize=(6, 6))
fig.subplots_adjust(left=0, right=1, bottom=0, top=1, hspace=0.05, wspace=0.05)
for i in range(64):
    ax = fig.add_subplot(8, 8, i + 1, xticks=[], yticks=[])#添加子图8*8
    ax.imshow(digits.images[i], cmap=plt.cm.binary)
    ax.text(0, 7, str(digits.target[i+m]), size=20)#真实值，标在左下角
    ax.text(6, 7, str(int(pre_test[i])), size=20, color="b")#预测值，标在右下角，蓝色显示
plt.show()