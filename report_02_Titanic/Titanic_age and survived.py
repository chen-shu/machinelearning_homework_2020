import os
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import csv
#sklearn中相关库
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import confusion_matrix,accuracy_score,roc_curve,auc,precision_recall_curve,average_precision_score

plt.rcParams['font.sans-serif'] = ['SimHei']
plt.rcParams['axes.unicode_minus'] = False

#确定数据文件的路径
home_path = "data"
train_path = os.path.join(home_path, "train.csv")

#读取数据文件
train_dataset = pd.read_csv(train_path)

#画图显示船票等级与幸存的关系
Survived_0 = train_dataset.Age[train_dataset.Survived == 0].value_counts()
Survived_1 = train_dataset.Age[train_dataset.Survived == 1].value_counts()
df=pd.DataFrame({'获救':Survived_1, '未获救':Survived_0})
df.plot(kind='bar', stacked=True)
plt.title("3个船票等级的获救情况")
plt.xlabel("船票等级") 
plt.ylabel("人数") 
plt.show()