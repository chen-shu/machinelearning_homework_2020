import os
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import csv
#sklearn中相关库
from sklearn.cluster import KMeans
from sklearn.metrics import confusion_matrix, accuracy_score

#显示中文
plt.rcParams['font.sans-serif'] = ['SimHei']
plt.rcParams['axes.unicode_minus'] = False

#确定数据文件的路径
home_path = "data"
train_path = os.path.join(home_path, "train.csv")
test_path = os.path.join(home_path, "test.csv")
submit_path = os.path.join(home_path, "gender_submission.csv")

#读取数据文件
train_dataset = pd.read_csv(train_path)
test_dataset = pd.read_csv(test_path)
submit_dataset = pd.read_csv(submit_path)

#剔除与乘客是否幸存无关或关系不大的数据（按列剔除）
train_dataset.drop(["PassengerId","Survived", "Name", "Cabin", "Ticket", "Embarked"], axis=1, inplace=True)
test_dataset.drop(["PassengerId", "Name", "Cabin", "Ticket", "Embarked"], axis=1, inplace=True)
survived_true = pd.read_csv("E:\\pyhomework\\machinelearning_homework\\report_02_Titanic\\data\\train.csv",usecols=['Survived'])

#量化“性别”，剔除空值所在行
train_dataset_pre = train_dataset.dropna(axis=0, how='any')
test_dataset_pre = test_dataset.dropna(axis=0, how='any')
train_dataset.replace("male",1,inplace=True)
train_dataset.replace("female",0,inplace=True)
test_dataset.replace("male",1,inplace=True)
test_dataset.replace("female",0,inplace=True)
ave_traindataset = sum(train_dataset_pre['Age']) / len(train_dataset_pre['Age'])
ave_testdataset = sum(test_dataset_pre['Age']) / len(test_dataset_pre['Age'])
train_dataset = train_dataset.fillna(ave_traindataset)
test_dataset = test_dataset.fillna(ave_testdataset)

#K-Means预测
survived_true = pd.read_csv(train_path,usecols=['Survived'])
est = KMeans(n_clusters=2, init="k-means++", n_jobs=-1)
est.fit(train_dataset, survived_true)

pred_train = est.predict(train_dataset) #训练数据的预测结果
pred_test = est.predict(test_dataset) #测试数据的预测结果

#预测后所得数据的处理
passenger = np.array(range(418))+892 #生成CaseId，方便后续和预测结果、结果概率等数据绑定后保存
passenger = pd.DataFrame(passenger,index=None,columns=['Passenger']) #把caseid由array转化为DataFrame
pred_test = pd.DataFrame(pred_test,index=None,columns=['Survived']) #把测试数据的预测结果由array转化为DataFrame
submit_data = pd.concat([passenger,pred_test],axis=1) #合并caseid和pred_test

#保存数据
submit_data.to_csv("submit_data_ave replace_kmeans.csv", index=False)

acc_train = accuracy_score(survived_true,pred_train)*100#准确率
print("训练数据的预测正确率 = %.2f" % acc_train,"%")

plt.figure(figsize=(10,3))
plt.scatter(submit_data['Passenger'],submit_data['Survived'])
plt.show()