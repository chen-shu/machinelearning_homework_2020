import pandas as pd
import numpy as np
import random
from matplotlib import pyplot as plt
import math

#读取数据
df = pd.read_csv('dataset_circles.csv',header=None,names=['x','y','class'])
df.to_csv('dataset_circles_new.csv',index=False)
dfc = pd.read_csv('dataset_circles_new.csv')#不能加“,header=0,index_col=0”，非则会出现KeyError: 'x'

#可视化数据
#title中显示中文
plt.rcParams['font.sans-serif'] = ['SimHei']
plt.rcParams['axes.unicode_minus'] = False
#可视化函数
def visualizeDataset(dfc,form1='c.',form2='m.'):
    plt.figure()
    for i in range(np.shape(dfc)[0]):
        if dfc['class'][i] == 0.0:
            plt.plot(dfc['x'][i],dfc['y'][i],form1)
        else:
            plt.plot(dfc['x'][i],dfc['y'][i],form2)
    plt.title('原始数据(按照class分类)')
    plt.xlabel('x')
    plt.ylabel('y')
    plt.show()
visualizeDataset(dfc)

class KMeans():
    
    def __init__(self, data, k=2):
        self.color = 'cmbyrkw'  #绘图颜色
        self.k = k #分为几类
        self.data = data #保存数据
        self.points = np.array([dfc['x'],dfc['y']]).T 
        self.clusters = {key:[]for key in range(self.k)}
        self.centroids = random.sample(list(self.points),k) #随机重心
    
    #欧式距离
    def __distance(self,point1,point2):
        #return np.sqrt(np.array(point1) - np.array(point2).sum())
        return math.sqrt(np.sum(np.power(point1-point2,2)))

    #计算重心
    def __calc_centroids(self,points):
        np_points = np.array(points)
        return np_points.mean(axis=0)

    #可视化绘图
    def __drawpoints(self,index,cluster,props='ro'):
        #绘制重心
        plt.plot(self.centroids[index][0],self.centroids[index][1],'*',c=self.color[index+2],ms=14)
        np_cluster = np.array(cluster)  # 转成numpy数组，方便绘图
        plt.plot(np_cluster[:,0],np_cluster[:,1],props) #绘图
        plt.xlabel('x')
        plt.ylabel('y')
        plt.grid(True)

    #计算是否满足停止条件
    def __stopable(self,index_list,index_list_his):
        self.count = self.count+1 #每迭代一次，做一次检查
        if len(index_list)!=len(index_list_his):
            return False
        np_index_list = np.array(index_list)
        np_index_list_his = np.array(index_list_his)
        return not np.any(np_index_list-np_index_list_his)

    #生成簇
    def __cluster(self):
        min_index_list = []
        while True:
            #清空点集，重新归类
            del self.clusters
            self.clusters = {key:[] for key in range(self.k)}
            min_index_list_his = min_index_list
            min_index_list = []
            for point in self.points:
            #for point in np.shape(self.points)[0]:
                # 计算所有点和重心距离，存入distance_array
                dis = np.zeros(self.k)
                #dis = np.array([])
                for i in range(self.k):
                    dis[i] = self.__distance(self.centroids[i], point)
                    #dis[i] = self.__distance(self.centroids[i], points[point][0],points[point][1])

                # 找出最小值的索引
                min_index = np.argmin(dis)
                min_index_list.append(min_index)
                # 按最小索引分类
                self.clusters[min_index].append(point)

            # 清空当前重心
            self.centroids.clear()
            # 重新计算新的重心
            for i in range(self.k):
                self.centroids.append(self.__calc_centroids(self.clusters[i]))
                # 绘图，可视化
                self.__drawpoints(i, self.clusters[i], props=self.color[i]+'.')
            plt.show()

            # 若无点的索引变化，则退出聚类循环
            if self.__stopable(min_index_list, min_index_list_his):
                return
    
    #返回最终结果
    def result(self):
        self.__cluster()

k=2
re = KMeans(data=dfc,k=k)
re.result()
#print('迭代了',iris.count,'次')