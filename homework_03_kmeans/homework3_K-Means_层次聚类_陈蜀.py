from sklearn import datasets
from sklearn.cluster import AgglomerativeClustering
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

plt.rcParams['font.sans-serif'] = ['SimHei']
plt.rcParams['axes.unicode_minus'] = False

k=int(input('k='))

df = pd.read_csv('dataset_circles.csv',header=None,names=['x','y','class'])#读取数据，加上表头
df.to_csv('dataset_circles_new.csv',index=False)#把加上表头的数据放到一个新的csv文件中
dff = pd.read_csv('dataset_circles_new.csv',usecols=['x', 'y'])#只读取x、y坐标，不读取类别"class"
dff.to_csv('dataset_circles_new.csv',index=False)#重新保存除去类别的文件
dfd = np.loadtxt(open("dataset_circles_new.csv","rb"),delimiter=",",skiprows=1)#以数组的形式读取数据，方便后续处理

X = []#空list，用来放置所有点的横坐标
y = []#空list，用来放置所有点的纵坐标
for p in dfd:
    X.append(p[0])
    y.append(p[1])

sc = AgglomerativeClustering(n_clusters=k,affinity='euclidean',linkage='single')#nearest_neighbors
sc_clusters2 = sc.fit_predict(dfd)#
plt.scatter(X,y, c=sc_clusters2)
plt.title("层次聚类聚类，k=2，linkage='single")
plt.show()