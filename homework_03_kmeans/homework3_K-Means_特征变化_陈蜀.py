import pandas as pd
import numpy as np
import random
from matplotlib import pyplot as plt
import math


#读取数据
df = pd.read_csv("dataset_circles.csv",header=None)

#坐标变换-极坐标
def coordinate_change(data):
    m = np.shape(data)[0]
    dff=pd.DataFrame(np.random.randn(400,2))
    for i in range(m):
       r = np.sqrt((data.iloc[i][0]**2) + (data.iloc[i][1]**2))#距离r，坐标的第一个参数
       theta = np.math.atan(data.iloc[i][1]/data.iloc[i][0])#角度theta角，坐标的第二个参数
       dff.iloc[i, :2] = r, theta
    return dff

#初始化聚类中心
def init(data,k):
    m = np.shape(data)[0] #样本数
    dataindex = list(range(m))#样本索引
    centerindex = random.sample(dataindex,k)#从列表中随机选择K个元素
    centerpoint = data.iloc[centerindex]
    k = np.mat(centerpoint)
    return k

#距离函数
def distance(point1,point2):
    return math.sqrt(np.sum(np.power(point1-point2,2)))

#聚类函数
def KMeans(data,k):
   m = np.shape(data)[0]  #样本数量
   cluster = np.mat(np.zeros((m,2)))
   centerpoint = init(data,k)  #初始化聚类中心
   changed = True 
   while changed:
       changed = False
       for i in range(m):
           dist = np.inf  #设置初始距离
           index = -1  #初始化索引值
           for j in range(k):
               dist_1 = distance(centerpoint[j,:],data.values[i,:])
               if dist_1 < dist:
                   dist = dist_1
                   index = j
           if cluster[i,0]!=index:
              changed = True
           cluster[i,:] = index,dist**2
        
       for i in range(k):
           pts = data.iloc[np.nonzero(cluster[:,0].A==i)[0]] 
           centerpoint[i,:] = np.mean(pts,axis=0)
   return cluster

#结果可视化
def result_show(data,result):
    plt.rcParams['font.sans-serif'] = ['SimHei']  #显示中文
    plt.rcParams['axes.unicode_minus'] = False
    plt.figure()
    color = 'cmbyrkw'
    m = np.shape(data)[0]
    for i in range (m):
        a = int(result[i,0])
        plt.scatter(data.iat[i,0], data.iat[i,1], color=color[a])
    plt.title("极坐标聚类结果")
    plt.xlabel('x')
    plt.ylabel('y')
    plt.grid(True)
    plt.show()
    

k=int(input('k='))
data = coordinate_change(df)
class_result = KMeans(data,k)
result_show(df,class_result,)